using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Snowball_Score : MonoBehaviour
{
    //scripts
    [SerializeField] private SnowballManager _snowballManager;

    //variables
    [SerializeField] private TextMeshProUGUI _scoreTextUI;
    [SerializeField] private int _score;
    public int score => _score;
    [SerializeField] private int _scoreOvertime;
    [SerializeField] private int _levelPassRequirement;
    public int levelPassRequirement => _levelPassRequirement;

    protected float _timer;
    private float _delay = 0.2f;

    private void Start()
    {
        _score = 0;
        _scoreOvertime = 1;
    }

    private void Update()
    {
        ScoreMinimumLimit();

        ScoreUIUpdate();

        ScoreIncreaseOvertime(_scoreOvertime);
    }

    private void ScoreMinimumLimit()
    {
        //Score not go below 0.
        if(_score <= 0)
        {
            _score = 0;
        }
    }

    private void ScoreUIUpdate()
    {
        switch(_snowballManager._milestoneCheck._milestonePassCheck)
        {
            case 0:
                {
                    _scoreTextUI.text = $"{_score}<color=#909090>/{_snowballManager._milestoneCheck.requirementMilestone1}</color>";
                    break;
                }
            case 1:
                {
                    _scoreTextUI.text = $"{_score}<color=#909090>/{_snowballManager._milestoneCheck.requirementMilestone2}</color>";
                    break;
                }
            case 2:
                {
                    _scoreTextUI.text = $"{_score}<color=#909090>/{_levelPassRequirement}</color>";
                    break;
                }
        }
    }

    private void ScoreIncreaseOvertime(int _scoreIncrease)
    {
        //Increase the timer.
        _timer += Time.deltaTime;

        //If the timer reaches the delay, increase score and reset timer.
        if(_timer >= _delay)
        {
            _timer = 0f;
            _score += _scoreIncrease;
        }
    }

    public void ScoreRateIncrease(int _increaseBy)
    {
        _scoreOvertime += _increaseBy;
    }

    public void ResetScoreRate()
    {
        _scoreOvertime = 1;
    }

    public void ScoreDecrease(int _decreaseBy)
    {
        _score -= _decreaseBy;
    }
}
