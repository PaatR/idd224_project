using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private static CameraController _instance;
    public static CameraController Instance { get { return _instance; } }

    public Transform target;
    public LayerMask aimMask;

    public Vector3 AimLocation;
    public float AimDistance = 100.0f;

    public float cameraDistance = 2f;

    public Vector3 anchorOffset;
    public Vector3 cameraOffset;

    public float cameraSmoothing = 1f;

    Camera cam;
    Vector3 refVector = Vector3.zero;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        cam = GetComponentInChildren<Camera>();
    }

    void Update()
    {
        if (target == null)
            return;

        cam.transform.localPosition = cameraOffset;
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.SmoothDamp(transform.position, target.position + anchorOffset,
            ref refVector, cameraSmoothing, Mathf.Infinity, Time.unscaledDeltaTime);

        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, AimDistance, aimMask))
        {
            AimLocation = hit.point;
        }
        else
        {
            AimLocation = cam.transform.forward * AimDistance;
        }
    }
}
