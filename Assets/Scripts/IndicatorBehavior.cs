using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IndicatorBehavior : MonoBehaviour
{
    [SerializeField] private float _fadeOutSpeed;
    [SerializeField] private TextMeshProUGUI _damageNum;
    private bool _startFading = false;

    private void Update()
    {
        gameObject.transform.position += new Vector3(0, 1, 0) * Time.deltaTime * _fadeOutSpeed;

        if(!_startFading)
        {
            StartCoroutine(FadeWithDelay(0.2f));
        }
        else if(_startFading)
        {
            _damageNum.color -= new Color32(0, 0, 0, 3);
        }
    }

    public void ChangeDamageNum(int _newDamageNum)
    {
        _damageNum.text = $"-{_newDamageNum}";
    }

    private IEnumerator FadeWithDelay(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        _startFading = true;
    }
}
