using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Snowball_Collision : MonoBehaviour
{
    //scripts
    [SerializeField] private SnowballManager _snowballManager;
    [SerializeField] private GameObject _scoreLostIndicator;

    private Vector3 _baseSnowballScale;

    private void Start()
    {
        _baseSnowballScale = _snowballManager._snowballMovement._snowballTransform.localScale;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SnowParticle"))
        {
            other.gameObject.SetActive(false);
            SizeUp();
            Debug.Log("Size is bigger now.");
        }

        if(other.gameObject.CompareTag("Obstacle"))
        {
            int _scorelost = other.gameObject.GetComponent<ObstacleScore>()._scoreDedection;

            other.gameObject.SetActive(false);
            _snowballManager._snowballScore.ScoreDecrease(_scorelost);

            DisplayScoreLostIndicator(_scorelost);
            Debug.Log($"Lose {_scorelost} score.");
        }

        //-------------------------------------------------------------------

        if(other.gameObject.CompareTag("FallChecker"))
        {
            _snowballManager._gameEndingManager._loseMessage.text = $"Defeated by gravity. Try again?";
            _snowballManager._gameEndingManager.Lose();
        }

        if(other.gameObject.CompareTag("GoalChecker"))
        {
            if(_snowballManager._snowballScore.score >= _snowballManager._snowballScore.levelPassRequirement)
            {
                _snowballManager._gameEndingManager.Win();
            }
            
            if(_snowballManager._snowballScore.score < _snowballManager._snowballScore.levelPassRequirement)
            {
                _snowballManager._gameEndingManager._loseMessage.text = $"You need more score. Try again?";
                _snowballManager._gameEndingManager.Lose();
            }
        }
    }

    private void SizeUp()
    {
        _snowballManager._snowballMovement._snowballTransform.localScale += new Vector3(0.2f, 0.2f, 0.2f);
        _snowballManager._snowballMovement.ThrustUp(1.15f);
        _snowballManager._snowballMovement.DirectionalSpeedDown(1.05f);

        _snowballManager._snowballScore.ScoreRateIncrease(1);
    }

    private void DisplayScoreLostIndicator(int _scoreLost)
    {
        GameObject _indicator = Instantiate(_scoreLostIndicator, new Vector3(75, 160, 0), Quaternion.identity, GameObject.Find("Canvas").transform);
        _indicator.SendMessage("ChangeDamageNum", _scoreLost);
        Destroy(_indicator, 0.5f);
    }

    public void ReturnToBaseSize()
    {
        _snowballManager._snowballMovement._snowballTransform.localScale = _baseSnowballScale;
        _snowballManager._snowballMovement.ResetThrust();
        _snowballManager._snowballMovement.ResetDirectionalMoveSpeed();
        _snowballManager._snowballScore.ResetScoreRate();
    }
}
