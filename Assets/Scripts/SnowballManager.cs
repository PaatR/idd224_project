using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowballManager : MonoBehaviour
{
    //scripts
    public Snowball_Movement _snowballMovement;
    public Snowball_Collision _snowballCollision;
    public Snowball_Score _snowballScore;
    public GameEndingManager _gameEndingManager;
    public MilestoneCheck _milestoneCheck;
}
