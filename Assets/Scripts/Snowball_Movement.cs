using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Snowball_Movement : MonoBehaviour
{
    //scripts
    [SerializeField] private SnowballManager _snowballManager;

    //variables
    public float _thrust {get; private set;}
    public float _directionalMoveSpeed { get; private set; }

    public Transform _snowballTransform;
    public Rigidbody _snowballRb;

    [SerializeField] private float _baseThrust;
    [SerializeField] private float _baseDirectionalMoveSpeed;

    private void Start()
    {
        _thrust = _baseThrust;
        _directionalMoveSpeed = _baseDirectionalMoveSpeed;
    }

    private void Update()
    {
        //Quick restart
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void FixedUpdate()
    {
        ForwardMovement();
        DirectionalMovement();
    }

    private void ForwardMovement()
    {
        //Check if already win/lose or not.
        if (_snowballManager._gameEndingManager.isWin == true || _snowballManager._gameEndingManager.isLose == true)
        {
            _snowballRb.velocity = Vector3.zero;
            Debug.Log("Forced stop snowball");
            return;
        }

        _snowballRb.AddForce(0, 0, _thrust * Time.deltaTime, ForceMode.VelocityChange);
        _snowballRb.velocity = new Vector3(0, _snowballRb.velocity.y, 0);
    }

    private void DirectionalMovement()
    {
        if (_snowballManager._gameEndingManager.isWin == true || _snowballManager._gameEndingManager.isLose == true)
        {
            return;
        }

        if (Input.GetKey(KeyCode.A))
        {
            _snowballTransform.position -= new Vector3(1f, 0f, 0f) * _directionalMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            _snowballTransform.position += new Vector3(1f, 0f, 0f) * _directionalMoveSpeed * Time.deltaTime;
        }
    }

    public void ThrustUp(float _multiplier)
    {
        _thrust = _thrust * _multiplier;
    }

    public void DirectionalSpeedDown(float _multiplier)
    {
        _directionalMoveSpeed = _directionalMoveSpeed / _multiplier;
    }

    public void ResetThrust()
    {
        _thrust = _baseThrust;
    }

    public void ResetDirectionalMoveSpeed()
    {
        _directionalMoveSpeed = _baseDirectionalMoveSpeed;
    }
}
