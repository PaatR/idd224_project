using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballBehavior : MonoBehaviour
{
    [SerializeField] float _fireballSpeed;
    private Transform _fireballTransform;
    private bool _canStartMoving;

    private void Start()
    {
        _fireballTransform = gameObject.GetComponent<Transform>();

        _canStartMoving = false;
        StartCoroutine(FireballSizeUp());
    }

    private void Update()
    {
        if(_canStartMoving)
        {
            _fireballTransform.position += new Vector3(0f, 0f, -1f) * _fireballSpeed * Time.deltaTime;
        }
    }

    private IEnumerator FireballSizeUp()
    {
        for (int i = 0; i < 30; i++)
        {
            //Debug.Log("fireball size up");
            _fireballTransform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
            yield return new WaitForSeconds(0.03f);
        }

        //Debug.Log("done size up");
        _canStartMoving = true;
    }
}
