using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameEndingManager : MonoBehaviour
{
    //scripts
    [SerializeField] private SnowballManager _snowballManager;

    //variables
    public bool isWin { get; private set; }
    public bool isLose { get; private set; }
    public TextMeshProUGUI _loseMessage;

    [SerializeField] private GameObject _winScreen;
    [SerializeField] private GameObject _loseScreen;

    private void Start()
    {
        isWin = false;
        isLose = false;
        _winScreen.SetActive(false);
        _loseScreen.SetActive(false);
    }

    private void Update()
    {
        if(isWin == true)
        {
            _winScreen.SetActive(true);
        }   
        if(isLose == true)
        {
            _loseScreen.SetActive(true);
        }
    }

    public void Win()
    {
        isWin = true;
    }

    public void Lose()
    {
        isLose = true;
    }
}
