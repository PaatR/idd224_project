using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlaceholderTutorialScript : MonoBehaviour
{
    [SerializeField] private GameObject _nextButton;
    [SerializeField] private GameObject _readyButton;

    [SerializeField] private GameObject _tt1;
    [SerializeField] private GameObject _tt2;
    [SerializeField] private GameObject _tt3;
    [SerializeField] private GameObject _tt4;

    [SerializeField] private TextMeshProUGUI _tt1Text;
    [SerializeField] private TextMeshProUGUI _tt2Text;
    [SerializeField] private TextMeshProUGUI _tt3Text;
    [SerializeField] private TextMeshProUGUI _tt4Text;

    [SerializeField] private Color32 _read;

    private int _ttCount;

    private void Start()
    {
        _nextButton.SetActive(true);
        _readyButton.SetActive(false);

        _ttCount = 1;

        _tt2.SetActive(false);
        _tt3.SetActive(false);
        _tt4.SetActive(false);
    }

    private void Update()
    {
        switch(_ttCount)
        {
            case 1:
                _tt1.SetActive(true);
                break;
            case 2:
                _tt1.SetActive(true);
                _tt1Text.color = _read;
                _tt2.SetActive(true);
                break;
            case 3:
                _tt1.SetActive(true);
                _tt1Text.color = _read;
                _tt2.SetActive(true);
                _tt2Text.color = _read;
                _tt3.SetActive(true);
                break;
            case 4:
                _tt1.SetActive(true);
                _tt1Text.color = _read;
                _tt2.SetActive(true);
                _tt2Text.color = _read;
                _tt3.SetActive(true);
                _tt3Text.color = _read;
                _tt4.SetActive(true);
                _nextButton.SetActive(false);
                _readyButton.SetActive(true);
                break;

        }
    }

    public void NextButtonPress()
    {
        _ttCount += 1;
    }

    public void ReadyButtonPress()
    {
        SceneManager.LoadScene("Main");
    }
}
