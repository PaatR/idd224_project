using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject _pauseUI;
    private bool _isPaused;

    private void Start()
    {
        DisablePause();
    }

    private void Update()
    {
        TogglePause();

        PauseUIActivation();
    }

    public void DisablePause()
    {
        _isPaused = false;
    }

    private void TogglePause()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!_isPaused)
            {
                _isPaused = true;
            }
            else if(_isPaused)
            {
                _isPaused = false;
            }
        }
    }

    private void PauseUIActivation()
    {
        if(_isPaused)
        {
            Time.timeScale = 0;
            _pauseUI.SetActive(true);
        }
        else if(!_isPaused)
        {
            Time.timeScale = 1;
            _pauseUI.SetActive(false);
        }
    }
}
