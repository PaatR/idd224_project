using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleFunctions : MonoBehaviour
{
    [Range(0,2)]
    [SerializeField] private int _obstacleCode;
    [SerializeField] private GameObject _fireballToSpawn;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            switch (_obstacleCode)
            {
                //CollapsingGround
                case 0:
                    Debug.Log("Collapsing Ground collided with player.");
                    this.gameObject.GetComponent<Renderer>().material.color = new Color32(94, 78, 60, 255);
                    StartCoroutine(StartCollapsing());
                    break;

                //Fireball
                case 1:
                    Debug.Log("Fireball Spawner collided with player.");
                    this.gameObject.GetComponent<Renderer>().material.color = new Color32(114, 64, 70, 255);
                    StartCoroutine(StartSpawningFireball());
                    break;
            }
        }
    }

    private IEnumerator StartCollapsing()
    {
        yield return new WaitForSeconds(0.5f);
        Debug.Log("Collapse Stage 1");
        this.gameObject.GetComponent<Renderer>().material.color = new Color32(63, 55, 50, 255);

        yield return new WaitForSeconds(0.5f);
        Debug.Log("Collapse Stage 2");
        this.gameObject.SetActive(false);
    }

    private IEnumerator StartSpawningFireball()
    {
        yield return new WaitForSeconds(0.05f);
        _fireballToSpawn.SetActive(true);
        Debug.Log("Fireball Spawned");
    }
}
