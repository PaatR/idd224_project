using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MilestoneCheck : MonoBehaviour
{
    [SerializeField] private SnowballManager _snowballManager;

    [Header("Milestone")]
    [SerializeField] private GameObject _milestone1;
    [SerializeField] private GameObject _milestone2;

    private Collider _collisionMilestone1;
    private Collider _collisionMilestone2;

    [Header("Requirement")]
    [SerializeField] private int _requirementMilestone1;
    public int requirementMilestone1 => _requirementMilestone1;
    [SerializeField] private int _requirementMilestone2;
    public int requirementMilestone2 => _requirementMilestone2;

    public int _milestonePassCheck;

    [Header("Skybox")]
    [SerializeField] private Image _fadeBlack;
    [SerializeField] private Material _skybox1;
    [SerializeField] private Material _skybox2;

    private void Start()
    {
        _collisionMilestone1 = _milestone1.GetComponent<BoxCollider>();
        _collisionMilestone2 = _milestone2.GetComponent<BoxCollider>();

        _milestonePassCheck = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == _collisionMilestone1)
        {
            if (_snowballManager._snowballScore.score >= _requirementMilestone1)
            {
                Debug.Log("MILE 1");
                StartCoroutine(ChangeSkybox(_skybox1));
                _milestonePassCheck = 1;
                //_snowballManager._snowballCollision.ReturnToBaseSize();
                return;
            }

            RestartScene();
        }
        if (other == _collisionMilestone2)
        {
            if (_snowballManager._snowballScore.score >= _requirementMilestone2)
            {
                Debug.Log("MILE 2");
                StartCoroutine(ChangeSkybox(_skybox2));
                _milestonePassCheck = 2;
                //_snowballManager._snowballCollision.ReturnToBaseSize();
                return;
            }

            RestartScene();
        }
    }

    private void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private IEnumerator ChangeSkybox(Material _newSkybox)
    {
        //color32 alpha up to 255
        for(int i = 0; i < 255; i++)
        {
            _fadeBlack.color += new Color32(0,0,0,1); 
            yield return new WaitForSeconds(0.001f);
        }

        //change skybox and reset size
        RenderSettings.skybox = _newSkybox;
        _snowballManager._snowballCollision.ReturnToBaseSize();

        //color32 alpha down to 0
        for (int i = 0; i < 255; i++)
        {
            _fadeBlack.color -= new Color32(0, 0, 0, 1);
            yield return new WaitForSeconds(0.001f);
        }
    }
}
